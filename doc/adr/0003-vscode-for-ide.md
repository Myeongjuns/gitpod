# 3. VSCode for IDE

Date: 2023-02-13

## Status

Superseded by [4. intelliJ is ide](0004-intellij-is-ide.md)

## Context

IDEs
vscode : free
intelli J : commercial
eclipse : free

vscode is most popular ide.

## Decision

We will use VSCode IDE.

## Consequences

What becomes easier or more difficult to do and any risks introduced by the change that will need to be mitigated.
