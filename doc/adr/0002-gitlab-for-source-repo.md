# 2. Gitlab for Source Repo

Date: 2023-02-13

## Status

Accepted

## Context

Github is not available for on-premice.
Gitlab can be used in on-premice.
Gitlab is open source.

## Decision

we decide Source Repo to Gitlab cloud.

## Consequences

We will use git cloud until MVP version.
Intra team will setup on-premice.
