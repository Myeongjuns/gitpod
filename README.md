# TTA GitPod VS Code


## Getting started

1. gitlab 가입
2. [TTA-CHAMP / Gitpod · GitLab](https://gitlab.com/tta-champ/gitpod) fork 주소
image.png
![fork.png](./fork.png)

3. gitlab 계정으로 gitpod.io 가입

![gitpod.png](./gitpod.png)

## docker Login
'docker login'

## docker build
- 'docker build .'

## docker push
- 'docker tag [tag_id] [dockerhub_id]/[name]:[version]'
  ## docker push 68cf7f612fe6 myeongjuns/builder:1.3
- 'docker rmi [tag_id] [dockerhub_id]/[name]:[version]'

- 'docker image ls'
- 'docker tag [tag_id] [dockerhub_id]/[name]:[version]'

## btop --utf-force

## docker login registry.gitlab.com
## docker build -t registry.gitlab.com/myeongjuns/gitpod .
## docker push registry.gitlab.com/myeongjuns/gitpod

##milestone
## https://www.projectlibre.com/
